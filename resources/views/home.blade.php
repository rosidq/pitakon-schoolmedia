<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="MYq51g7lyvAgNdwhRHbK3NYG6LiIJp0pKmzGGNhH">
    <title>Selamat Datang - Tanya Jawab Schoolmedia</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://bootswatch.com/3/lumen/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://pitakon.schoolmedia.id/css/style.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="landing-page-body">
        <div class="header">
            <img src="https://pitakon.schoolmedia.id/css/img/logo.png" alt="Logo">
            <ul>
                <li><a href="#">Tentang Schoolmedia</a></li>
                <li><a href="#">Promo</a></li>
            </ul>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 welcome">
                    <div class="big">Tanya Jawab</div>
                    <div class="strip"></div>
                    <p class="mt-5">
                        Merupakan aplikasi yang memfasilitasi seluruh pengguna Schoolmedia
                        untuk menanyakan suatu pertanyaan dan/atau menjawab suatu pertanyaan.
                    </p>
                    <a href="{{ route('user.pengaruhmu.index') }}" class="btn btn-success">Masuk</a>
                    <a href="#" class="btn btn-warning">Pelajari</a>
                </div>
            </div>
        </div>

        <div class="footer">
            &copy 2019 Schoolmedia. All rights reserved
        </div>
    </div>

    <!-- SCRIPT -->
    <script src="https://pitakon.schoolmedia.id/js/script.js" charset="utf-8"></script>
</body>

</html>