<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Question;

class PitakonController extends Controller
{
    //Partisipasi
    public function partisipasi()
    {
        $data['partisipasi'] = Question::all();
        
        return view('admin.partisipasi.index', $data);
    }

    // Histori
    public function histori()
    {
        $data['histori'] = Question::all();

        return view('admin.histori.index', $data);
    }
}
