<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Question;

class PengaruhmuController extends Controller
{
    public function index()
    {
        $data['pitakon'] = Question::all();

        return view('user.pengaruhmu.index', $data);
    }
}
