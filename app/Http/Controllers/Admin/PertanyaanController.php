<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Model\Question;

class PertanyaanController extends Controller
{
    // Pertanyaan Tertuju Ke Personal
    public function pertanyaan_personal()
    {
        $data['pitakon'] = Question::all();

        return view('admin.pertanyaan.personal', $data);
    }

    // Pertanyaan Tertuju Ke Bursa
    public function pertanyaan_bursa()
    {
        $data['pitakon'] = Question::all();

        return view('admin.pertanyaan.bursa', $data);
    }
}
