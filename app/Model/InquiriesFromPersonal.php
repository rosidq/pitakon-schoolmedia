<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InquiriesFromPersonal extends Model
{
    protected $table = "inquiries_from_personal";
}
