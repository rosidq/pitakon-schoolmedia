<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <title>Pitakon 3B</title>
</head>
<body>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg" style="background: -webkit-linear-gradient(0deg, #98b1c3 19.5%, #014282 19.5%);">
        <div class="col-sm-2" style="width: 20%!important;">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="https://schoolmedia.id/assets/img/site-logo.png" style="width: 100%;">
                </a>
            </div>
        </div>
        <div class="col-sm-9" style="width: 80%!important;">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <h5>Pitakon</h5>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                          <a class="nav-link text-white pr-3 pl-3" href="#">
                              <img src="assets/icon/notif.svg" width="20px" height="20px">
                          </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-white pr-3 pl-3" href="#">
                                <img src="assets/icon/inbox.svg" width="20px" height="20px">
                            </a>
                          </li>
                        <li class="nav-item">
                          <a class="nav-link text-white pr-3 pl-3" href="pitakonb1">
                              <img src="assets/icon/home.svg" width="20px" height="20px">
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-white pr-3 pl-3" href="pitakon1" style="margin-top: 2px;"><b>Peter</b></a>
                        </li>
                   </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- Content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-lg-3" style="width: 20%!important; padding-right: 0px; padding-left: 0px;">
                
                <nav id="sidebarMenu" class="d-md-block bg-white sidebar collapse">
                    <div class="container">
                        <div class="position-sticky pt-4">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="assets/img/bdd.png" class="rounded-circle" width="65px" height="65px;">
                                </div>
                                <div class="col-sm-8 mt-2">
                                    <a class="text-schoolmedia" style="text-decoration: none; font-size: 18px;" href="#">
                                        <b>Petter Watson</b>
                                    </a>
                                    <p style="font-size: 13px; font-weight: bold; color: #9d9d9d;">
                                        SMAN 1 Klaten
                                    </p>
                                </div>
                            </div>
                            
                            <h6 class="sidebar-heading d-flex justify-content-center px-3 mt-4 mb-1 text-schoolmedia">
                                PENCAPAIAN
                            </h6>
                            <div class="row">
                                <div class="div mt-3 mb-3">
                                    <ul class="nav nav-pills" id="pills-tab" role="tablist" style="font-size: 15px; font-weight: bold;">
                                        <li class="nav-item col-7" role="presentation">
                                          <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">7 Hari Terakhir</a>
                                        </li>
                                        <li class="nav-item col-5" role="presentation">
                                          <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">Seluruhnya</a>
                                        </li>
                                    </ul>     
                                </div>
                                  <div class="tab-content mb-3  " id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        Bintangmu selama 7 Hari terakhir
    
                                        <div class="row">
                                            <div class="col-sm-2 mt-3">
                                                <img src="assets/icon/jawaban.svg" width="40px" height="40px;">
                                            </div>
                                            <div class="col-sm-4 mt-3">
                                                <h6 style="margin-top: 12px; margin-left: 5px;">Jawaban</h6>
                                            </div>
                                            <div class="col-sm-6 mt-3 d-flex justify-content-end">
                                                <h6 style="margin-top: 12px;">15</h6>
                                            </div>
                                        </div>
    
                                        <div class="row">
                                            <div class="col-sm-2 mt-3">
                                                <img src="assets/icon/popular.svg" width="40px" height="40px;">
                                            </div>
                                            <div class="col-sm-4 mt-3">
                                                <h6 style="margin-top: 12px; margin-left: 5px;">Popularitas</h6>
                                            </div>
                                            <div class="col-sm-6 mt-3 d-flex justify-content-end">
                                                <h6 style="margin-top: 12px;">15</h6>
                                            </div>
                                        </div>
    
                                        <div class="row">
                                            <div class="col-sm-2 mt-3">
                                                <img src="assets/icon/brain.svg" width="40px" height="40px;">
                                            </div>
                                            <div class="col-sm-4 mt-3">
                                                <h6 style="margin-top: 12px; margin-left: 5px;">Brainliest</h6>
                                            </div>
                                            <div class="col-sm-6 mt-3 d-flex justify-content-end">
                                                <h6 style="margin-top: 12px;">15</h6>
                                            </div>
                                        </div>
    
                                        <div class="row">
                                            <div class="col-sm-2 mt-3">
                                                <img src="assets/icon/heart.svg" width="40px" height="40px;">
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <h6 style="margin-top: 12px; margin-left: 5px;">Terima Kasih</h6>
                                            </div>
                                            <div class="col-sm-4 mt-3 d-flex justify-content-end">
                                                <h6 style="margin-top: 12px;">15</h6>
                                            </div>
                                        </div>
    
                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        Bintangmu Keseluruhan
                                    
                                        <div class="row">
                                            <div class="col-sm-2 mt-3">
                                                <img src="assets/icon/jawaban.svg" width="40px" height="40px;">
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <h6 style="margin-top: 12px; margin-left: 5px;">Jawaban</h6>
                                            </div>
                                            <div class="col-sm-4 mt-3 d-flex justify-content-end">
                                                <h6 style="margin-top: 12px;">15</h6>
                                            </div>
                                        </div>
    
                                        <div class="row">
                                            <div class="col-sm-2 mt-3">
                                                <img src="assets/icon/popular.svg" width="40px" height="40px;">
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <h6 style="margin-top: 12px; margin-left: 5px;">Popularitas</h6>
                                            </div>
                                            <div class="col-sm-4 mt-3 d-flex justify-content-end">
                                                <h6 style="margin-top: 12px;">15</h6>
                                            </div>
                                        </div>
    
                                        <div class="row">
                                            <div class="col-sm-2 mt-3">
                                                <img src="assets/icon/brain.svg" width="40px" height="40px;">
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <h6 style="margin-top: 12px; margin-left: 5px;">Brainliest</h6>
                                            </div>
                                            <div class="col-sm-4 mt-3 d-flex justify-content-end">
                                                <h6 style="margin-top: 12px;">15</h6>
                                            </div>
                                        </div>
    
                                        <div class="row">
                                            <div class="col-sm-2 mt-3">
                                                <img src="assets/icon/heart.svg" width="40px" height="40px;">
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <h6 style="margin-top: 12px; margin-left: 5px;">Terima Kasih</h6>
                                            </div>
                                            <div class="col-sm-4 mt-3 d-flex justify-content-end">
                                                <h6 style="margin-top: 12px;">15</h6>
                                            </div>
                                        </div>
    
                                    </div>
                                    
                                  </div>
                            </div>
                    
                        </div>
                    </div>
                </nav>


                <nav id="sidebarMenu" class="mt-2 d-md-block bg-white sidebar collapse">
                    <div class="container">
                        <div class="position-sticky pt-2 pb-2">
                            <div class="row">
                                <div class="col-sm-2 mt-3">
                                    <img src="assets/icon/question.svg" width="40px" height="40px;">
                                </div>
                                <div class="col-sm-8 mt-3">
                                    <h6 style="margin-top: 12px; margin-left: 5px;">Pertanyaan Terjawab</h6>
                                </div>
                                <div class="col-sm-2 mt-3 d-flex justify-content-end">
                                    <h6 style="margin-top: 12px;">15</h6>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-sm-2 mt-3">
                                    <img src="assets/icon/question.svg" width="40px" height="40px;">
                                </div>
                                <div class="col-sm-8 mt-2">
                                    <h6 style="margin-top: 12px; margin-left: 5px;">Pertanyaan Belum Terjawab</h6>
                                </div>
                                <div class="col-sm-2 mt-2 d-flex justify-content-end">
                                    <h6 style="margin-top: 12px;">15</h6>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-sm-6 mt-3">
                                    <h6>Poin</h6>
                                </div>
                                <div class="col-sm-6 mt-3 d-flex justify-content-end">
                                    <h6>25</h6>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-sm-6 mt-3">
                                    <h6>SC</h6>
                                </div>
                                <div class="col-sm-6 mt-3 d-flex justify-content-end">
                                    <h6>15</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>


                <nav id="sidebarMenu" class="mt-2 d-md-block bg-white sidebar collapse">
                    <div class="container">
                        <div class="position-sticky pt-2 pb-2">
                            <div class="row">
                                <div class="col-sm-12 mt-3 d-flex justify-content-center">
                                    <h6>14 Mei - 20 Mei</h6>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 d-flex justify-content-center">
                                    <img src="assets/icon/jawaban.svg" width="50px" height="50px">
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-sm-12 mt-3 d-flex justify-content-center">
                                    <p style="font-size: 12px; font-weight: bold; color: #9d9d9d;">Mulai Menjawab Pertanyaan dan Belajar</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>

            </div>
            

            
        
            <main class="col-md-9 col-lg-9 px-md-4" style="width: 79.3%!important; padding-right: 0px!important; padding-left: 10px!important;">
                <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
                    
                    <div class="row">
                        <div class="col-sm-12 d-flex justify-content-end mt-4">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link text-dark" href="pitakon1"><b>Pengaruhmu</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark" href="pitakon2a"><b>Pertanyaan</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active bg-warning" href="pitakon3"><b>Jawaban</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark" href="pitakon4b"><b>Partisipasi</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark" href="pitakon5"><b>Histori</b></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <label for="form-select">Kategori</label>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Kurikulum/Tema</option>
                                <option value="1">14 Hari</option>
                                <option value="2">1 Bulan</option>
                                <option value="3">1 Tahun</option>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <label for="form-select">Jenjang</label>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>SMA/SMK/MA</option>
                                <option value="1">SMP/MTS</option>
                                <option value="2">SD/MI</option>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <label for="form-select">ID Pertanyaan</label>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Semua</option>
                                <option value="1">SMP/MTS</option>
                                <option value="2">SD/MI</option>
                            </select>
                        </div>
                    </div>
                    
                </div>


                <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
                    
                    <div class="col-sm-12 d-flex justify-content-center mt-4">
                        <table class="table table-striped">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Waktu Buat</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Jenjang</th>
                                <th scope="col">Pertanyaan</th>
                                <th scope="col">Username</th>
                                <th scope="col">Nama</th>
                                <th scope="col">JU</th>
                                <th scope="col">Scoin</th>
                                <th scope="col">Poin</th>
                                <th scope="col">Menjawab</th>
                                <th scope="col">Terpilih</th>
                                <th scope="col">Dibaca</th>
                                <th scope="col">Respon</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th scope="row">1</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>GU</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-secondary">Jawab</a>
                                </td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                                    </div>
                                </td>
                                <td>43</td>
                                <td>10</td>
                              </tr>
                              <tr>
                                <th scope="row">2</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>GU</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-secondary">Jawab</a>
                                </td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                                    </div>
                                </td>
                                <td>43</td>
                                <td>10</td>
                              </tr>
                              <tr>
                                <th scope="row">3</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>GU</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-secondary">Jawab</a>
                                </td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                                    </div>
                                </td>
                                <td>43</td>
                                <td>10</td>
                              </tr>
                              <tr>
                                <th scope="row">4</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>GU</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-secondary">Jawab</a>
                                </td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                                    </div>
                                </td>
                                <td>43</td>
                                <td>10</td>
                              </tr>
                              <tr>
                                <th scope="row">5</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>GU</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-secondary">Jawab</a>
                                </td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                                    </div>
                                </td>
                                <td>43</td>
                                <td>10</td>
                              </tr>
                              
                            </tbody>
                        </table>
                    </div>
                    
                </div>


            </main>
        </div>

    </section>

    



    <!-- JS -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</body>
</html>