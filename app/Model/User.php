<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";

    public function question()
    {
        return $this->belongsTo('App\Model\Question');
    }
}
