<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Question;

class PertanyaanController extends Controller
{
    public function index()
    {
        $data['pitakon'] = Question::all();

        return view('user.pertanyaan.index', $data);
    }

    public function create()
    {
        return view('user.pertanyaan.create');
    }
}
