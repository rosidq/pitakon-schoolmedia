<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.png">
    <title>Pitakon  |  Schoolmedia</title>
    {{-- CSS Files --}}
    <link href="/assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="/assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="/assets/dist/css/style.min.css" rel="stylesheet">
    @yield('styles-user')
</head>
<body>
<div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
    <header class="topbar" data-navbarbg="skin6">
        <nav class="navbar top-navbar navbar-expand-md" style="background: #98b1c3!important;">
            <div class="navbar-header" data-logobg="skin6">
                <a cla ss="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                    <i class="ti-menu ti-close"></i></a>
                <div class="navbar-brand">
                    <a href="index.html">
                        <span class="logo-text">
                            <img src="https://schoolmedia.id/assets/img/site-logo.png" alt="homepage" style="width: 100%;" class="dark-logo" />
                            <img src="/assets/images/logo-light-text.png" class="light-logo" alt="homepage" />
                        </span>
                    </a>
                </div>
                <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                    data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                        class="ti-more"></i></a>
            </div>
            <div class="navbar-collapse collapse" id="navbarSupportedContent" style="background: #014282; border: 1px solid #014282;">
                <ul class="navbar-nav float-left mr-auto ml-4 pt-2 pl-1">
                    <li>
                        <h2 style="color: #f1b400;"><b><i>Pitakon</i></b></h2>
                    </li>
                </ul>
                <ul class="navbar-nav float-right">
                    <li class="nav-item d-none d-md-block">
                        <a class="nav-link" href="javascript:void(0)">
                            <form>
                                <div class="customize-input">
                                    <input class="form-control custom-shadow custom-radius border-0 bg-white" type="search" placeholder="Search" aria-label="Search">
                                    <i class="form-control-icon" data-feather="search"></i>
                                </div>
                            </form>
                        </a>
                    </li>

                    <!-- Notification -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle pl-md-3 position-relative" href="javascript:void(0)"
                            id="bell" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <span><i data-feather="bell" class="svg-icon"></i></span>
                            <span class="badge badge-primary notify-no rounded-circle">5</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left mailbox animated bounceInDown">
                            <ul class="list-style-none">
                                <li>
                                    <div class="message-center notifications position-relative">
                                        <!-- Message -->
                                        <a href="javascript:void(0)"
                                            class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                            <div class="btn btn-danger rounded-circle btn-circle"><i
                                                    data-feather="airplay" class="text-white"></i></div>
                                            <div class="w-75 d-inline-block v-middle pl-2">
                                                <h6 class="message-title mb-0 mt-1">Luanch Admin</h6>
                                                <span class="font-12 text-nowrap d-block text-muted">Just see
                                                    the my new
                                                    admin!</span>
                                                <span class="font-12 text-nowrap d-block text-muted">9:30 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="javascript:void(0)"
                                            class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                            <span class="btn btn-success text-white rounded-circle btn-circle"><i
                                                    data-feather="calendar" class="text-white"></i></span>
                                            <div class="w-75 d-inline-block v-middle pl-2">
                                                <h6 class="message-title mb-0 mt-1">Event today</h6>
                                                <span
                                                    class="font-12 text-nowrap d-block text-muted text-truncate">Just
                                                    a reminder that you have event</span>
                                                <span class="font-12 text-nowrap d-block text-muted">9:10 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="javascript:void(0)"
                                            class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                            <span class="btn btn-info rounded-circle btn-circle"><i
                                                    data-feather="settings" class="text-white"></i></span>
                                            <div class="w-75 d-inline-block v-middle pl-2">
                                                <h6 class="message-title mb-0 mt-1">Settings</h6>
                                                <span
                                                    class="font-12 text-nowrap d-block text-muted text-truncate">You
                                                    can customize this template
                                                    as you want</span>
                                                <span class="font-12 text-nowrap d-block text-muted">9:08 AM</span>
                                            </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="javascript:void(0)"
                                            class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                            <span class="btn btn-primary rounded-circle btn-circle"><i
                                                    data-feather="box" class="text-white"></i></span>
                                            <div class="w-75 d-inline-block v-middle pl-2">
                                                <h6 class="message-title mb-0 mt-1">Pavan kumar</h6> <span
                                                    class="font-12 text-nowrap d-block text-muted">Just
                                                    see the my admin!</span>
                                                <span class="font-12 text-nowrap d-block text-muted">9:02 AM</span>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link pt-3 text-center text-dark" href="javascript:void(0);">
                                        <strong>Check all notifications</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- End Notification -->

                    <li class="nav-item">
                        <a class="nav-link" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="/assets/icon/rupiah.svg" width="20px" height="20px">
                        </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i data-feather="settings" class="svg-icon"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="beranda.html" role="button" aria-haspopup="true" aria-expanded="false">
                            <i data-feather="home" class="svg-icon"></i>
                        </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <img src="/assets/images/users/profile-pic.jpg" alt="user" class="rounded-circle"
                                width="40">
                            <span class="ml-2 d-none d-lg-inline-block">
                                <span>User-Schoolmedia</span>
                                <i data-feather="chevron-down" class="svg-icon"></i>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                            <a class="dropdown-item" href="javascript:void(0)">
                                <i data-feather="user" class="svg-icon mr-2 ml-1"></i>
                                My Profile
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)">
                                <i data-feather="credit-card" class="svg-icon mr-2 ml-1"></i>
                                My Balance
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)">
                                <i data-feather="mail" class="svg-icon mr-2 ml-1"></i>
                                Inbox
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)">
                                <i data-feather="settings" class="svg-icon mr-2 ml-1"></i>
                                Account Setting
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)">
                                <i data-feather="power" class="svg-icon mr-2 ml-1"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="left-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="sidebar-item"> 
                        <div class="row">
                            <div class="col-4">
                                <a class="sidebar-link" href="#" aria-expanded="false" style="opacity: 1;">
                                    <img src="/assets/images/users/profile-pic.jpg" alt="user" class="rounded-circle" width="50">
                                </a>
                            </div>
                            <div class="col-8">
                                <div class="row mt-3">
                                    <div class="col-12">
                                        <span style="color: #014282;"><b>User-Schoolmedia</b></span>
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 12px;"><p>SMKN 1 Dlanggu</p></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-divider"></li>
                    <li class="nav-small-cap"><span class="hide-menu">Pencapaian</span></li>
                </ul>

                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="padding-left: 25px; padding-right: 25px;">
                    <li class="nav-item">
                      <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="font-size: 10px;">7 Hari Sebelumnya</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="font-size: 10px;">Seluruhnya</a>
                    </li>
                </ul>

                <div class="tab-content" id="pills-tabContent" style="padding-right: 25px; padding-left: 25px;">
                    <!-- Tab 7 Hari -->
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <ul id="sidebarnav" style="padding: 0!important;">
                
                            <span class="hide-menu mb-5" style="font-size: 12px;">Bintangmu selama 7 hari terakhir</span>

                            <li class="sidebar-item">
                                <div class="row">
                                    <div class="col-8">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html"
                                        aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;"><i data-feather="message-square" class="feather-icon"></i><span
                                        class="hide-menu">Jawaban</span></a>
                                    </div>
                                    <div class="col-4">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html"
                                        aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;"><span
                                        class="hide-menu">56</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-8">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <i data-feather="calendar" class="feather-icon"></i>
                                            <span class="hide-menu">Popularitas</span>
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">15</span>
                                        </a>
                                    </div>
                                    <div class="col-10 mb-2 ml-4" style="margin-top: -10px;">
                                        <span class="hide-menu" style="font-size: 10px;">Jumlah orang yang belajar dari jawabanmu</span>
                                    </div>
                                </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-8">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <i data-feather="calendar" class="feather-icon"></i>
                                            <span class="hide-menu">Brainliest</span>
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                    <div class="col-10 mb-2 ml-4" style="margin-top: -10px;">
                                        <span class="hide-menu" style="font-size: 10px;">Jawabanmu memang yang paling top!</span>
                                    </div>
                                </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-8">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <i data-feather="calendar" class="feather-icon"></i>
                                            <span class="hide-menu">Terima Kasih</span>
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                    <div class="col-10 mb-2 ml-4" style="margin-top: -10px;">
                                        <span class="hide-menu" style="font-size: 10px;">Coba menolong yang lain, ini menyenangkan!</span>
                                    </div>
                                </div>
                            </li>

                            <li class="list-divider"></li>
                            
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-10">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <i data-feather="calendar" class="feather-icon"></i>
                                            <span class="hide-menu">Terjawab</span>
                                        </a>
                                    </div>
                                    <div class="col-2">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-10">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <i data-feather="calendar" class="feather-icon"></i>
                                            <span class="hide-menu">Belum Terjawab</span>
                                        </a>
                                    </div>
                                    <div class="col-2">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-10">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <img src="/assets/icon/poin.svg" width="20px" height="20px">
                                            <span class="hide-menu ml-2">Poin</span>
                                        </a>
                                    </div>
                                    <div class="col-2">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-10">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <img src="/assets/icon/rupiah.svg" width="20px" height="20px">
                                            <span class="hide-menu ml-2">Scoin</span>
                                        </a>
                                    </div>
                                    <div class="col-2">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <!-- End Tab 7 Hari -->
                    
                    <!-- Tab Seluruhnya -->
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <ul id="sidebarnav" style="padding: 0!important;">
                            <span class="hide-menu mb-5" style="font-size: 12px;">Bintangmu seluruhnya</span>
                            <li class="sidebar-item">
                                    <div class="row">
                                        <div class="col-8">
                                            <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                                <i data-feather="message-square" class="feather-icon"></i>
                                                <span class="hide-menu">Jawaban</span>
                                            </a>
                                        </div>
                                        <div class="col-4">
                                            <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                                <span class="hide-menu">156</span>
                                            </a>
                                        </div>
                                    </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-8">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <i data-feather="calendar" class="feather-icon"></i>
                                            <span class="hide-menu">Popularitas</span>
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-8">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <i data-feather="calendar" class="feather-icon"></i>
                                            <span class="hide-menu">Brainliest</span>
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="sidebar-item"> 
                                <div class="row">
                                    <div class="col-8">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px;">
                                            <i data-feather="calendar" class="feather-icon"></i>
                                            <span class="hide-menu">Terima Kasih</span></a>
                                    </div>
                                    <div class="col-4">
                                        <a class="sidebar-link sidebar-link" href="app-calendar.html" aria-expanded="false" style="font-size: 14px; padding: 0; padding-bottom: 8px; float: right;">
                                            <span class="hide-menu">50</span>
                                        </a>
                                    </div>
                                </div>
                            </li>
    
                        </ul>
                    </div>
                    <!-- End Tab Seluruhnya -->
                </div>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
</div>

        {{-- Main Content --}}
            @yield('content-user')
        {{-- End Main Content --}}
    
</body>

{{-- General JS Script --}}
<script src="/assets/libs/jquery/dist/jquery.min.js"></script>
<script src="/assets/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- apps -->
<!-- apps -->
<script src="/assets/dist/js/app-style-switcher.js"></script>
<script src="/assets/dist/js/feather.min.js"></script>
<script src="/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="/assets/dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="/assets/dist/js/custom.min.js"></script>
<!--This page JavaScript -->
<script src="/assets/extra-libs/c3/d3.min.js"></script>
<script src="/assets/extra-libs/c3/c3.min.js"></script>
<script src="/assets/libs/chartist/dist/chartist.min.js"></script>
<script src="/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
<script src="/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
<script src="/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
<script src="/assets/dist/js/pages/dashboards/dashboard1.min.js"></script>
@yield('script-user')

</html>