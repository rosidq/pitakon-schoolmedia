<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Question;

class JawabanController extends Controller
{
    // Jawaban Dari Personal
    public function jawaban_personal()
    {
        $data['pitakon'] = Question::all();

        return view('admin.jawaban.personal', $data);
    }

    // Jawaban Dari Bursa
    Public function jawaban_bursa()
    {
        $data['pitakon'] = Question::all();

        return view('admin.jawaban.bursa', $data);
    }

    // Detail Jawaban
    public function detail_jawaban()
    {
        $data['detail'] = Question::all();

        return view('admin.jawaban.detail_jawaban', $data);
    }
}
