<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CurriculumCategory extends Model
{
    protected $table = "curriculum_category";
}
