<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersonalInquiries extends Model
{
    protected $table = "personal_inquiries";
}
